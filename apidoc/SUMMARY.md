# Summary

- [Create a booking](./chapters/create-a-booking.md)
- [Fetch all bookings](./chapters/fetch-all-bookings.md)
- [Fetch bookings by status](./chapters/fetch-bookings-by-status.md)
- [Accept a booking request](./chapters/accept-a-booking-request.md)
- [Generate Invoice for a booking](./chapters/generate-invoice-for-a-booking.md)






















