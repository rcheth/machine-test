# Generate Invoice for a booking
> This POST method API generates an invoice for a booking made by user.
------
## Headers
| KEY | Value |
|-----|------|
| URL | **`/v1/bookings/:bookingId/payment`** |
| METHOD | **POST** |
| content-type | **application/json** |
------
## Request
```javascript
{}

```
------
## Success
```javascript
{
    "success": true,
    "modified": true
}

```
------
## Error
```javascript
{
    "error": "CastError: Cast to ObjectId failed for value \"618911471994763521f346a\" at path \"_id\" for model \"bookings\""
}
```