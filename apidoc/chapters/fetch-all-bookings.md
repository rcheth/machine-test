# Fetch All Booking
> This GET method API returns all bookings made by user. The API is paginated and returns 5 records for every page number. Default page number is 1.
------
## Headers
| KEY | Value |
|-----|------|
| URL | **`/v1/bookings/:mobile/:pageNumber`** |
| METHOD | **GET** |
------
## Success
```javascript
[
    {
        "_id": "618911471994763521f346ca",
        "booking_status": "PENDING",
        "mobile": "9425012823",
        "createdAt": "2021-11-08T12:00:08.004Z",
        "updatedAt": "2021-11-08T12:00:08.004Z",
        "__v": 0
    },
    {
        "_id": "6189114b1994763521f346cb",
        "booking_status": "PENDING",
        "mobile": "9425012823",
        "createdAt": "2021-11-08T12:00:11.280Z",
        "updatedAt": "2021-11-08T12:00:11.280Z",
        "__v": 0
    },
    {
        "_id": "6189114c1994763521f346cc",
        "booking_status": "PENDING",
        "mobile": "9425012823",
        "createdAt": "2021-11-08T12:00:12.329Z",
        "updatedAt": "2021-11-08T12:00:12.329Z",
        "__v": 0
    },
    {
        "_id": "6189114d1994763521f346cd",
        "booking_status": "PENDING",
        "mobile": "9425012823",
        "createdAt": "2021-11-08T12:00:13.241Z",
        "updatedAt": "2021-11-08T12:00:13.241Z",
        "__v": 0
    },
    {
        "_id": "6189114e1994763521f346ce",
        "booking_status": "PENDING",
        "mobile": "9425012823",
        "createdAt": "2021-11-08T12:00:14.212Z",
        "updatedAt": "2021-11-08T12:00:14.212Z",
        "__v": 0
    }
]

```
------
## Error
```javascript
[]
```