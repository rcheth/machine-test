# Fetch Booking by Status
> This GET method API returns a status-filtered bookings made by user. The API is paginated and returns 5 records for every page number. Default page number is 1. Possible values for Status = PENDING, ACTIVE, PAYMENT 
------
## Headers
| KEY | Value |
|-----|------|
| URL | **`/v1/bookings/:mobile/status/:status/:page`** |
| METHOD | **GET** |
------
## Success
```javascript
[
    {
        "_id": "618911471994763521f346ca",
        "booking_status": "PAYMENT",
        "mobile": "9425012823",
        "createdAt": "2021-11-08T12:00:08.004Z",
        "updatedAt": "2021-11-08T12:01:05.939Z",
        "__v": 0,
        "invoice_generated": true
    }
]

```
------
## Error
```javascript
[]
```