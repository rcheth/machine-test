import * as express from "express";
import AppInit from "./AppInit";
import appConfig from "./config/appConfig";
console.log(`Starting webdura:${process.env.NODE_ENV || "development"}`);
let app = express();
AppInit.initializeHelmet(app);
AppInit.initializeCors(app);
AppInit.initializeDB();
AppInit.initializeExpressApp(app);
AppInit.initializeRoutes(app);
AppInit.errorHandler(app);
let port = appConfig.get("port");

let server = app.listen(port, () => {
  console.log("server started at port", port);
  if (process.send) {
    process.send("online");
  }
});
