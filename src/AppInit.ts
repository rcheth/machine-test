import * as express from "express";
import * as helmet from "helmet";
import * as mongoose from "mongoose";
import * as compression from "compression";
import Routes from "./routes/Routes";
import CorsHelper from "./zlib/Cors";
import DBConnection from "./zlib/DBConnection";
import AppConfigUtil from "./zlib/AppConfigUtil";
import { urlencoded, json } from "body-parser";

export default class AppInit {
  static initializeDB(): Promise<any> {
    return new Promise((resolve, reject) => {
      (<any>mongoose).Promise = Promise;
      let dbName = AppConfigUtil.get(`db:name`);
      DBConnection.connect(dbName).then((db) => {
        resolve(db);
      }, reject);
    });
  }
  static initializeHelmet(app: express.Application) {
    app.use("/v1*", helmet());
  }
  static initializeCors(app: express.Application) {
    return CorsHelper.initializeCors(app);
  }
  static errorHandler(app: express.Application) {
    app.use((err, req, res, next) => {
      res.send({ error: err.message || err.toString() });
      next();
    });
  }
  static initializeExpressApp(app: express.Application) {
    app.set("trust proxy", true);
    app.use(urlencoded({ extended: true }));
    app.use(compression() as any);
    app.use(json());
  }
  static initializeRoutes(app: express.Application) {
    return Routes.init(app);
  }
}
