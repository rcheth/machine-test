import BookingModel from "../models/BookingModel";
import MongoUtil from "./MongoUtil";
import { BookingStatus } from "./AppConstants";

export default class BookingHelper {
    private static async save(bookingBody: any): Promise<any> {
        return await new BookingModel(bookingBody).save();
    }
    static newBooking(body: any): Promise<any> {
        return new Promise(function (resolve, reject) {
            body = body || {};
            body.booking_status = BookingStatus.PENDING;
            BookingHelper.save(body).then(
                (newBooking) => {
                    resolve(newBooking);
                },
                (err) => {
                    reject(new Error(MongoUtil.errMesg(err)))
                },
            );
        });
    }
    static fetchBookings(params): Promise<any> {
        return new Promise(function (resolve, reject) {
            let page = Number(params.page) >= 1 ? Number(params.page) : 1;
            page = page - 1
            BookingModel
                .find({ mobile: params.mobile })
                .sort({ _id: "asc" })
                .limit(5)
                .skip(5 * page)
                .lean()
                .exec()
                .then(
                    (results) => {
                        resolve(results);
                    }, (err) => {
                        reject(err);
                    }
                );
        })
    }
    static fetchBookingsByStatus(params): Promise<any> {
        return new Promise(function (resolve, reject) {
            let page = Number(params.page) >= 1 ? Number(params.page) : 1;
            page = page - 1
            let status = params.status || "pending";
            status = status.toUpperCase();
            BookingModel
                .find({ mobile: params.mobile })
                .sort({ _id: "asc" })
                .limit(5)
                .skip(5 * page)
                .lean()
                .exec()
                .then((result) => {
                    result = result.filter(b => b.booking_status === status)
                    resolve(result);
                }, (err) => {
                    reject(err);
                });
        })
    }
    static acceptBooking(id: any): Promise<any> {
        return new Promise(function (resolve, reject) {
            BookingModel.update({ _id: id }, { $set: { booking_status: BookingStatus.ACTIVE } }, { upsert: false })
                .exec()
                .then(
                    (result) => {
                        result.n === 1
                            ? result.nModified === 1
                                ? resolve({
                                    success: true,
                                    modified: true
                                })
                                : resolve({
                                    success: true,
                                    modified: false
                                })
                            : resolve({
                                success: false,
                                modified: false
                            });
                    },
                    (err) => {
                        reject(err);
                    },
                );
        });
    }
    static generateInvoice(id: any): Promise<any> {
        return new Promise(function (resolve, reject) {
            BookingModel.update({ _id: id }, { $set: { booking_status: BookingStatus.PAYMENT, invoice_generated: true } }, { upsert: false })
                .exec()
                .then(
                    (result) => {
                        result.n === 1
                            ? result.nModified === 1
                                ? resolve({
                                    success: true,
                                    modified: true
                                })
                                : resolve({
                                    success: true,
                                    modified: false
                                })
                            : resolve({
                                success: false,
                                modified: false
                            });
                    },
                    (err) => {
                        reject(err);
                    },
                );
        })
    }
}