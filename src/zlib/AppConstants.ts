export var BookingStatus = {
    PENDING: "PENDING",
    ACTIVE: "ACTIVE",
    PAYMENT: "PAYMENT"
};