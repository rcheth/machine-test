export default class MongoUtil {
  static errMesg(err: any): string {
    let fieldErrors = (err && err.errors) || {};
    let message = err.message;
    for (let key in fieldErrors) {
      message = (fieldErrors[key] && fieldErrors[key].message) || message;
    }
    return message;
  }
}
