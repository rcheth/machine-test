import * as express from "express";
import BookingController from "../controllers/BookingController";

export default class BookingRoutes {
  static init(app: express.Application) {
    app.post("/v1/bookings", BookingController.createBooking);
    app.get("/v1/bookings/:mobile/:page?", BookingController.fetchBookings);
    app.get("/v1/bookings/:mobile/status/:status/:page?", BookingController.fetchBookingsByStatus);
    app.put("/v1/bookings/:id/active", BookingController.acceptBooking);
    app.post("/v1/bookings/:id/payment", BookingController.generateInvoice);
  }
}
