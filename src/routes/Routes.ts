import * as express from "express";
import BookingRoutes from "./BookingRoutes";

export default class Routes {
  static init(app: express.Application) {
    BookingRoutes.init(app);
  }
}
